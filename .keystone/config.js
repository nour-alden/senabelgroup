"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// keystone.ts
var keystone_exports = {};
__export(keystone_exports, {
  default: () => keystone_default
});
module.exports = __toCommonJS(keystone_exports);
var import_core7 = require("@keystone-6/core");
var import_auth = require("@keystone-6/auth");

// src/config.ts
var import_session = require("@keystone-6/core/session");

// src/schemas/index.ts
var schemas_exports = {};
__export(schemas_exports, {
  Admin: () => Admin,
  Category: () => Category,
  Log: () => Log,
  Order: () => Order,
  Product: () => Product,
  Section: () => Section
});

// src/schemas/Admin.schema.ts
var import_core = require("@keystone-6/core");
var import_fields2 = require("@keystone-6/core/fields");

// src/commonFields.ts
var import_fields = require("@keystone-6/core/fields");
var commonFields = () => ({
  createBy: (0, import_fields.relationship)({
    ref: "Admin",
    hooks: {
      resolveInput: ({ context, operation }) => {
        return operation === "create" && context?.session?.itemId ? { connect: { id: context?.session?.itemId } } : void 0;
      }
    }
  }),
  updateBy: (0, import_fields.relationship)({
    ref: "Admin",
    hooks: {
      resolveInput: ({ context }) => context?.session?.itemId ? {
        connect: { id: context?.session?.itemId }
      } : void 0
    }
  }),
  updateAt: (0, import_fields.timestamp)({
    hooks: {
      resolveInput: () => (/* @__PURE__ */ new Date()).toISOString()
    }
  }),
  createAt: (0, import_fields.timestamp)({ defaultValue: { kind: "now" } })
});
var CommonFieldsList = Object.keys(
  commonFields
);

// src/schemas/Admin.schema.ts
var Admin = (0, import_core.list)({
  fields: {
    name: (0, import_fields2.text)({}),
    phone: (0, import_fields2.text)({ isIndexed: "unique" }),
    password: (0, import_fields2.password)({}),
    ...commonFields()
  },
  access: {
    operation: {
      query: ({ session: session2 }) => !!session2,
      create: ({ session: session2 }) => !!session2,
      update: ({ session: session2 }) => !!session2,
      delete: ({ session: session2 }) => !!session2
    }
  },
  ui: {
    label: "\u0627\u0644\u0645\u0633\u062A\u062E\u062F\u0645\u064A\u0646",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "phone",
        "updateBy",
        "updateAt"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});

// src/schemas/Category.schema.ts
var import_core2 = require("@keystone-6/core");
var import_fields3 = require("@keystone-6/core/fields");
var Category = (0, import_core2.list)({
  fields: {
    name: (0, import_fields3.text)({ validation: { isRequired: true }, isIndexed: "unique" }),
    description: (0, import_fields3.text)({ ui: { displayMode: "textarea" } }),
    avatar: (0, import_fields3.image)({ storage: "image" }),
    section: (0, import_fields3.relationship)({ ref: "Section.category" }),
    product: (0, import_fields3.relationship)({ ref: "Product.category", many: true }),
    isActive: (0, import_fields3.checkbox)({ defaultValue: true }),
    ...commonFields()
  },
  access: {
    operation: {
      query: () => true,
      create: ({ session: session2 }) => !!session2,
      update: ({ session: session2 }) => !!session2,
      delete: ({ session: session2 }) => !!session2
    }
  },
  ui: {
    label: "\u0627\u0644\u0627\u0635\u0646\u0627\u0641",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "description",
        "avatar",
        "section",
        "isActive",
        "product",
        "updateBy",
        "updateAt"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});

// src/schemas/Log.schema.ts
var import_core3 = require("@keystone-6/core");
var import_fields4 = require("@keystone-6/core/fields");
var Log = (0, import_core3.list)({
  fields: {
    listKey: (0, import_fields4.text)(),
    operation: (0, import_fields4.text)(),
    inputData: (0, import_fields4.json)(),
    originalItem: (0, import_fields4.json)(),
    item: (0, import_fields4.json)(),
    resolvedData: (0, import_fields4.json)(),
    admin: (0, import_fields4.relationship)({
      ref: "Admin",
      hooks: {
        resolveInput: ({ context }) => context.session?.data?.id ? { connect: { id: context.session.data.id } } : void 0
      }
    }),
    createAt: (0, import_fields4.timestamp)({ defaultValue: { kind: "now" } })
  },
  access: {
    operation: {
      query: ({ session: session2 }) => !!session2,
      create: () => false,
      update: () => false,
      delete: () => false
    }
  },
  ui: {
    labelField: "listKey",
    listView: {
      initialColumns: [
        "listKey",
        "operation",
        "inputData",
        "originalItem",
        "item",
        "resolvedData",
        "admin",
        "createAt"
      ]
    }
  }
});

// src/schemas/Order.schema.ts
var import_core4 = require("@keystone-6/core");
var import_fields5 = require("@keystone-6/core/fields");
var Order = (0, import_core4.list)({
  fields: {
    phoneNumber: (0, import_fields5.text)({}),
    total: (0, import_fields5.text)({ validation: { isRequired: true } }),
    notes: (0, import_fields5.text)({ ui: { displayMode: "textarea" } }),
    products: (0, import_fields5.text)({ ui: { displayMode: "textarea" } }),
    ...commonFields()
  },
  access: {
    operation: {
      query: ({ session: session2 }) => !!session2,
      create: () => true,
      update: ({ session: session2 }) => !!session2,
      delete: ({ session: session2 }) => !!session2
    }
  },
  ui: {
    label: "\u0627\u0644\u0637\u0644\u0628\u0627\u062A",
    labelField: "phoneNumber",
    listView: {
      initialColumns: [
        "phoneNumber",
        "total",
        "notes",
        "products",
        "updateAt",
        "createAt",
        "createBy",
        "updateBy"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});

// src/schemas/Product.schema.ts
var import_core5 = require("@keystone-6/core");
var import_fields6 = require("@keystone-6/core/fields");
var Product = (0, import_core5.list)({
  fields: {
    name: (0, import_fields6.text)({ validation: { isRequired: true }, isIndexed: "unique" }),
    description: (0, import_fields6.text)({ ui: { displayMode: "textarea" } }),
    avatar: (0, import_fields6.image)({ storage: "image" }),
    price: (0, import_fields6.float)({ defaultValue: 0, validation: { min: 0 } }),
    category: (0, import_fields6.relationship)({ ref: "Category.product", many: true }),
    isActive: (0, import_fields6.checkbox)({ defaultValue: true }),
    ...commonFields()
  },
  access: {
    operation: {
      query: () => true,
      create: ({ session: session2 }) => !!session2,
      update: ({ session: session2 }) => !!session2,
      delete: ({ session: session2 }) => !!session2
    }
  },
  ui: {
    label: "\u0627\u0644\u0645\u0646\u062A\u062C\u0627\u062A",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "description",
        "avatar",
        "price",
        "isActive",
        "category",
        "updateBy",
        "updateAt"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});

// src/schemas/Section.schema.ts
var import_core6 = require("@keystone-6/core");
var import_fields7 = require("@keystone-6/core/fields");
var Section = (0, import_core6.list)({
  fields: {
    name: (0, import_fields7.text)({ isIndexed: "unique", validation: { isRequired: true } }),
    category: (0, import_fields7.relationship)({ ref: "Category.section", many: true }),
    ...commonFields()
  },
  access: {
    operation: {
      query: () => true,
      create: ({ session: session2 }) => !!session2,
      update: ({ session: session2 }) => !!session2,
      delete: ({ session: session2 }) => !!session2
    }
  },
  ui: {
    label: "\u0627\u0644\u0627\u0642\u0633\u0627\u0645",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "category",
        "updateBy",
        "updateAt"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});

// src/config.ts
var session = (0, import_session.statelessSessions)({
  maxAge: 60 * 60 * 24 * 30,
  secret: "test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test"
});
var config_default = {
  session,
  db: {
    provider: "sqlite",
    url: "file:./de.db"
  },
  server: {
    port: 4500,
    healthCheck: {
      path: "/health",
      data: () => ({
        status: "healthy",
        timestamp: Date.now()
      })
    },
    cors: { origin: "*" }
  },
  storage: {
    image: {
      kind: "local",
      type: "image",
      generateUrl: (path) => `http://13.38.156.138:4500/images${path}`,
      serverRoute: {
        path: "/images"
      },
      storagePath: "public/images"
    }
  },
  lists: schemas_exports
};

// keystone.ts
var { withAuth } = (0, import_auth.createAuth)({
  listKey: "Admin",
  identityField: "phone",
  secretField: "password",
  initFirstItem: {
    fields: ["phone", "password"],
    skipKeystoneWelcome: true
  },
  sessionData: `
    id
    name
    phone
  `
});
var keystone_default = withAuth((0, import_core7.config)(config_default));
//# sourceMappingURL=config.js.map
