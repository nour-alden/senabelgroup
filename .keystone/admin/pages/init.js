import { getInitPage } from '@keystone-6/auth/pages/InitPage';

const fieldPaths = ["phone","password"];

export default getInitPage({"listKey":"Admin","fieldPaths":["phone","password"],"enableWelcome":false});
