import { getSigninPage } from '@keystone-6/auth/pages/SigninPage'

export default getSigninPage({"identityField":"phone","secretField":"password","mutationName":"authenticateAdminWithPassword","successTypename":"AdminAuthenticationWithPasswordSuccess","failureTypename":"AdminAuthenticationWithPasswordFailure"});
