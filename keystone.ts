import { config } from "@keystone-6/core";
import { createAuth } from "@keystone-6/auth";
import configData from "./src/config";

const { withAuth } = createAuth({
  listKey: "Admin",
  identityField: "phone",
  secretField: "password",
  initFirstItem: {
    fields: ["phone", "password"],
    skipKeystoneWelcome: true
  },
  sessionData: `
    id
    name
    phone
  `
});

export default withAuth(config(configData));
