import { list } from "@keystone-6/core";
import { relationship, text } from "@keystone-6/core/fields";
import { commonFields } from "../commonFields";

export const Section = list({
  fields: {
    name: text({ isIndexed: "unique", validation: { isRequired: true } }),
    category: relationship({ ref: "Category.section", many: true }),
    ...commonFields()
  },
  access: {
    operation: {
      query: () => true,
      create: ({ session }) => !!session,
      update: ({ session }) => !!session,
      delete: ({ session }) => !!session
    }
  },
  ui: {
    label:"الاقسام",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "category",
        "updateBy",
        "updateAt",
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});
