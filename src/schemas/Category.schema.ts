import { list } from "@keystone-6/core";
import { image, text, relationship, checkbox } from "@keystone-6/core/fields";
import { commonFields } from "../commonFields";

export const Category = list({
  fields: {
    name: text({ validation: { isRequired: true }, isIndexed: "unique" }),
    description: text({ ui: { displayMode: "textarea" } }),
    avatar: image({ storage: "image" }),
    section: relationship({ ref: "Section.category" }),
    product: relationship({ ref: "Product.category", many: true }),
    isActive: checkbox({ defaultValue: true }),

    ...commonFields()
  },

  access: {
    operation: {
      query: () => true,
      create: ({ session }) => !!session,
      update: ({ session }) => !!session,
      delete: ({ session }) => !!session
    }
  },
  ui: {
    label:"الاصناف",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "description",
        "avatar",
        "section",
        "isActive",
        "product",
        "updateBy",
        "updateAt",
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});
