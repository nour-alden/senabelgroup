import { list } from "@keystone-6/core";
import {
  image,
  float,
  relationship,
  text,
  checkbox
} from "@keystone-6/core/fields";
import { commonFields } from "../commonFields";

export const Product = list({
  fields: {
    name: text({ validation: { isRequired: true }, isIndexed: "unique" }),
    description: text({ ui: { displayMode: "textarea" } }),
    avatar: image({ storage: "image" }),
    price: float({ defaultValue: 0, validation: { min: 0 } }),
    category: relationship({ ref: "Category.product", many: true }),
    isActive: checkbox({ defaultValue: true }),

    ...commonFields()
  },
  access: {
    operation: {
      query: () => true,
      create: ({ session }) => !!session,
      update: ({ session }) => !!session,
      delete: ({ session }) => !!session
    }
  },
  ui: {
    label:"المنتجات",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "description",
        "avatar",
        "price",
        "isActive",
        "category",
        "updateBy",
        "updateAt",
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});
