import { list } from "@keystone-6/core";
import { password, text } from "@keystone-6/core/fields";
import { commonFields } from "../commonFields";

export const Admin = list({
  fields: {
    name: text({}),
    phone: text({ isIndexed: "unique" }),
    password: password({}),
    ...commonFields()
  },
  access: {
    operation: {
      query: ({ session }) => !!session,
      create: ({ session }) => !!session,
      update: ({ session }) => !!session,
      delete: ({ session }) => !!session
    }
  },
  ui: {
    label:"المستخدمين",
    labelField: "name",
    listView: {
      initialColumns: [
        "name",
        "phone",
        "updateBy",
        "updateAt",
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});
