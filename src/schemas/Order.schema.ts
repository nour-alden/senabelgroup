import { list } from "@keystone-6/core";
import { text } from "@keystone-6/core/fields";
import { commonFields } from "../commonFields";

export const Order = list({
  fields: {
    phoneNumber: text({}),
    total: text({ validation: { isRequired: true } }),
    notes: text({ ui: { displayMode: "textarea" } }),
    products: text({ ui: { displayMode: "textarea" } }),
    ...commonFields()
  },
  access: {
    operation: {
      query: ({ session }) => !!session,
      create: () => true,
      update: ({ session }) => !!session,
      delete: ({ session }) => !!session
    }
  },
  ui: {
    label:"الطلبات",
    labelField: "phoneNumber",
    listView: {
      initialColumns: [
        "phoneNumber",
        "total",
        "notes",
        "products",
        "updateAt",
        "createAt",
        "createBy",
        "updateBy"
      ]
    }
  },
  hooks: {
    afterOperation: async ({
      context,
      listKey,
      operation,
      inputData,
      originalItem,
      item,
      resolvedData
    }) => {
      await context.sudo().query.Log.createOne({
        data: {
          listKey,
          operation,
          inputData,
          originalItem,
          item,
          resolvedData
        }
      });
    }
  }
});
