import { list } from "@keystone-6/core";
import { timestamp, json, text, relationship } from "@keystone-6/core/fields";

export const Log = list({
  fields: {
    listKey: text(),
    operation: text(),
    inputData: json(),
    originalItem: json(),
    item: json(),
    resolvedData: json(),
    admin: relationship({
      ref: "Admin",
      hooks: {
        resolveInput: ({ context }) =>
          context.session?.data?.id
            ? { connect: { id: context.session.data.id } }
            : undefined
      }
    }),
    createAt: timestamp({ defaultValue: { kind: "now" } })
  },
  access: {
    operation: {
      query: ({ session }) => !!session,
      create: () => false,
      update: () => false,
      delete: () => false
    }
  },
  ui: {

    labelField: "listKey",
    listView: {
      initialColumns: [
        "listKey",
        "operation",
        "inputData",
        "originalItem",
        "item",
        "resolvedData",
        "admin",
        "createAt"
      ]
    }
  }
});
