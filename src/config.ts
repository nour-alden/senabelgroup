import { statelessSessions } from "@keystone-6/core/session";
import * as lists from "./schemas";
import { KeystoneConfig } from "@keystone-6/core/src/types/index";

export const session = statelessSessions({
  maxAge: 60 * 60 * 24 * 30,
  secret:
    "test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test_test"
});

export default {
  session,
  db: {
    provider: "sqlite",
    url: "file:./de.db"
  },
  server: {
    port: 4500,
    healthCheck: {
      path: "/health",
      data: () => ({
        status: "healthy",
        timestamp: Date.now()
      })
    },
    cors: { origin: "*" }
  },
  storage: {
    image: {
      kind: "local",
      type: "image",
      generateUrl: path => `http://13.38.156.138:4500/images${path}`,
      serverRoute: {
        path: "/images"
      },
      storagePath: "public/images"
    }
  },

  lists
} as KeystoneConfig;
