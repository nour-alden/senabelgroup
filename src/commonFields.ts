import { relationship, timestamp } from "@keystone-6/core/fields";

export const commonFields = () => ({
  createBy: relationship({
    ref: "Admin",
    hooks: {
      resolveInput: ({ context, operation }) => {
        return operation === "create" && context?.session?.itemId
          ? { connect: { id: context?.session?.itemId } }
          : undefined;
      }
    }
  }),
  updateBy: relationship({
    ref: "Admin",
    hooks: {
      resolveInput: ({ context }) =>
        context?.session?.itemId
          ? {
              connect: { id: context?.session?.itemId }
            }
          : undefined
    }
  }),
  updateAt: timestamp({
    hooks: {
      resolveInput: () => new Date().toISOString()
    }
  }),
  createAt: timestamp({ defaultValue: { kind: "now" } })
});

export type CommonFields = keyof typeof commonFields;

export const CommonFieldsList: CommonFields[] = Object.keys(
  commonFields
) as CommonFields[];
